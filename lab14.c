#include <stdio.h>
#include <string.h>

#define MAX_PAG 100

#define MAX 101

/* Funcao: contemPalavras
 *
 * Parametros:
 *   s: string contendo o texto de entrada
 *   vs: vetor de strings com as palavras a serem localizadas
 *   n: numero de palavras
 * 
 * Descricao:
 *   Deve-se procurar pelas ocorrencias das palavras em vs na string s.
 *
 * Retorno:
 *   1 caso todas ocorrencias forem encontradas, caso contrario 0
 */
int contemPalavras(char s[], char vs[][MAX], int n) {
	/* Implementar a funcao e trocar o valor de retorno */
	return 0;
}

/* Funcao: removePalavras
 *
 * Parametros:
 *   s: string contendo o texto de entrada
 *   vs: vetor de strings com as palavras a serem removidas
 *   n: numero de palavras
 *
 * Descricao:
 *   Deve-se remover as palavras de s que estiverem listadas em vs.
 *   Ao final, s nao deve conter espacos extras.
 */
void removePalavras(char s[], char vs[][MAX], int n) {
	/* Implementar a funcao */
}

/* Funcao: pagsResposta
 *
 * Parametros:
 *   paginas: vetor de strings cada uma representando uma pagina
 *   numPag: numero de paginas
 *   termosBusca: vetor de strings com os termos a serem buscados
 *   numTer: numero de termos a serem buscados
 *   resp: vetor a ser preenchido como resposta, de dimensao numPag
 *
 * Descricao:
 *   Deve-se procurar cada palavra em termosBusca em cada pagina do vetor paginas.
 *   Se todas as palavras foram encontradas na pagina i, resp[i] deve ser atualizado com 1 (verdadeiro).
 *   Caso contrario, resp[i] deve ser 0.
 *   
 */
void pagsResposta(char paginas[][MAX], int numPag, char termosBusca[][MAX], int numTer, int resp[]) {
	/* Implementar a funcao */
}

/* Funcao: linksResposta
 *
 * Parametros:
 *   links: matriz quadrada binária representando links entre as paginas 
 *   numPag: numero de paginas (dimensoes da matriz de links)
 *   resp: vetor obtido apos execucao de pagsResposta
 *   numLinks: vetor a ser preenchido como resposta, de dimensao numPag
 *
 * Descricao:
 *   Deve-se preencher o vetor numLinks da seguinte maneira: para cada
 *   posicao i (0 <= i < numPags), se resp[i] == 1, então numLinks[i] deve conter
 *   o numero de links de outras paginas resposta para i. Caso resp[i] == 0,
 *   entao numLinks[i] deve ser -1.
 *
 */
void linksResposta(int links[MAX_PAG][MAX_PAG], int numPag, int resp[], int numLinks[]) {
	/* Implementar a funcao */
}
