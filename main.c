#include <stdio.h>
#include <string.h>

#define MAX_PAG 100
#define MAX_PAL 20
#define MAX 101


void removePalavras(char s[], char vs[][MAX], int n);

void pagsResposta(char paginas[][MAX], int numPag, char termosBusca[][MAX], int numTer, int resp[]);

void linksResposta(int links[MAX_PAG][MAX_PAG], int numPag, int resp[], int numLinks[]);

int contemPalavras(char s[], char vs[][MAX], int n);

int main() {
	int i, j, origem, destino, numeroLinks;
	int numeroPaginas, numeroPalavrasBuscadas, numeroPalavrasIgnorar;
	int paginasTemPalavrasChave[MAX_PAG];
	int numeroLinksPorPagina[MAX_PAG];

	char palavrasPagina[MAX_PAG][MAX];
	char buscaOriginal[MAX], busca[MAX], aux[MAX];
	char palavrasBusca[MAX_PAL][MAX];
	char palavrasIgnorar[MAX_PAL][MAX];
	char *token;
	
	int links[MAX_PAG][MAX_PAG];

	// le string a ser procurada
	if (fgets(busca, MAX, stdin) != busca)
		return 0;
	busca[strlen(busca)-1] = '\0'; // remove fim de linha lido pelo fgets
	strcpy (buscaOriginal, busca);
	
	// le palavras a serem ignoradas (stop words)
	if (fgets(aux, MAX, stdin) != aux)
		return 0;
	token = strtok (aux," ");
	numeroPalavrasIgnorar = 0;
	while (token != NULL) {
		strcpy(&palavrasIgnorar[numeroPalavrasIgnorar][0], token);
		if (palavrasIgnorar[numeroPalavrasIgnorar][strlen(palavrasIgnorar[numeroPalavrasIgnorar])-1] == '\n')
			palavrasIgnorar[numeroPalavrasIgnorar][strlen(palavrasIgnorar[numeroPalavrasIgnorar])-1] = '\0';
		numeroPalavrasIgnorar++;
		token = strtok (NULL," ");
	}

	// le numero de paginas
	if (fgets(aux, MAX, stdin) != aux)
		return 0;
	sscanf(aux, "%d", &numeroPaginas);

	// le palavras em cada página
	for (i = 0; i < numeroPaginas; i++) {
		if (fgets(palavrasPagina[i], MAX, stdin) != palavrasPagina[i])
			return 0;
		palavrasPagina[i][strlen(palavrasPagina[i])-1] = '\0';
	}

	// leitura dos links
	for (i = 0; i < numeroPaginas; i++)
	for (j = 0; j < numeroPaginas; j++) {
		links[i][j] = 0;
	}

	// le links
	if (scanf("%d", &numeroLinks) != 1) {
		return 0;
	}
	for (i = 0; i < numeroLinks; i++) {
		if (scanf("%d %d", &origem, &destino) != 2) {
			return 0;
		}
		links[origem][destino] = 1;
	}

	// processa para retirar palavras nao importantes
	removePalavras(busca, palavrasIgnorar, numeroPalavrasIgnorar);

	// Informacoes na saida
	printf("Termo a ser buscado: %s\n", buscaOriginal);
	printf("Termo a ser buscado processado: %s\n",busca);

	// transforma busca em array de palavras
	token = strtok (busca," ");
	numeroPalavrasBuscadas = 0;
	while (token != NULL) {
		strcpy(palavrasBusca[numeroPalavrasBuscadas], token);
		numeroPalavrasBuscadas++;
		token = strtok (NULL," ");
	}

	// verifica quais palavras-chaves estao em quais paginas
	pagsResposta(palavrasPagina, numeroPaginas, palavrasBusca,
		numeroPalavrasBuscadas, paginasTemPalavrasChave);

	//  calcula numero de links por pagina
	linksResposta(links, numeroPaginas, paginasTemPalavrasChave, numeroLinksPorPagina);

	for (i = 0; i < numeroPaginas; i++) {
		if (numeroLinksPorPagina[i] == -1)
			printf("Pagina %d: nao encontrado\n", i);
		else
			printf("Pagina %d: encontrado/%d links\n", i, numeroLinksPorPagina[i]);
	}

	return 0;
}
